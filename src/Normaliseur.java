
/**
  * Normaliseur normalise les données d'un lot en utilisant une transformation affine.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Normaliseur extends Traitement {

	private double debut = 0;
	private double fin = 0;

	private double a;
	private double b;
	
	private Max maximum;
	private Max minimum;
	private Donnees donnees;
	
	
	public Normaliseur(double debut, double fin) {
		this.debut = debut;
		this.fin = fin;
		
		this.maximum = new Max();
		this.minimum = new Max();
		this.donnees = new Donnees();
	}
	
	
	@Override
	public void traiter(Position position, double valeur) {
		this.maximum.traiter(position, valeur);
		this.minimum.traiter(position, -valeur);
		this.donnees.traiter(position, valeur);
		// cr�er une chaine de traitement
		// utiliser mulitiplicateur pour "-valeur"
		
		super.traiter(position, valeur);
	}
	
	@Override
    protected void gererDebutLotLocal(String nomLot) {
        this.donnees.gererDebutLot("normalise");
    }
	
	@Override
	protected void gererFinLotLocal(String nomLot) {
		this.donnees.gererFinLot("normalise");
		double M = this.maximum.max();
		double m = -this.minimum.max();
		
		this.a = (M - m)/(this.fin - this.debut);
		this.b = this.debut - this.a*m;
		
		for (int i = 0; i < this.donnees.getValeurList().size(); i++) {
            this.donnees.getValeurList().set(i, a * this.donnees.getValeurList().get(i) + b);
        }
		//super.traiter(position, valeur);
			
	}
}

