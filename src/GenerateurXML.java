import java.io.FileOutputStream;
import java.io.IOException;

import org.jdom2.Attribute;
import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * GenerateurXML écrit dans un fichier, à charque fin de lot, toutes
 * les données lues en indiquant le lot dans le fichier XML.
 *
 * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class GenerateurXML extends Traitement {

	private String fileName;
	private Element racine;
	private Element lotElement;
	private Document doc;
	private Donnees donnees;

	public GenerateurXML(String fileName) {
		this.fileName = fileName;
		this.racine = new Element("Resultat"); 
		this.doc = new Document(this.racine, new DocType("generateur.dtd"));
	}
	
	@Override
	public void gererDebutLotLocal(String nomLot) {
		this.donnees = new Donnees();
		this.donnees.gererDebutLot("Donnees " + nomLot);
		this.lotElement = new Element("Lot");
		this.lotElement.setAttribute("nom", nomLot);
		this.racine.addContent(this.lotElement);
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		this.donnees.traiter(position, valeur);	
		super.traiter(position, valeur);
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		this.donnees.gererFinLot("Data " + nomLot);
		
		for (int i = 0; i < this.donnees.getPositionList().size(); i++) {
			Position position = this.donnees.getPositionList().get(i);
			double value = this.donnees.getValeurList().get(i);
			
			Element elemPosition = new Element("Position");
			
			elemPosition.setAttribute(new Attribute("x", String.valueOf(position.x)));
			elemPosition.setAttribute(new Attribute("y", String.valueOf(position.y)));

			Element valeur = new Element("valeur");
			valeur.setText(String.valueOf(value));
			elemPosition.addContent(valeur);
			this.lotElement.addContent(elemPosition);
		}
		
	    XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
		try {
			FileOutputStream docOutput = new FileOutputStream(this.fileName);
			xmlOutput.output(this.doc, docOutput);
			docOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
}
