import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class InterfaceSwing extends JFrame{

	final private JLabel abs;
	final private JLabel ord;
	final private JLabel val;
	final private JTextField absField;
	final private JTextField ordField;
	final private JTextField valField;
	final private JButton bValider;
	final private JButton bEffacer;
	final private JButton bTerminer;
	
	private ArrayList<String> absList = new ArrayList<String>();
	private ArrayList<String> ordList = new ArrayList<String>();
	private ArrayList<String> valList = new ArrayList<String>();
	
	public InterfaceSwing(String titre) {
		
		this.abs = new JLabel("Abscisse");
		this.ord = new JLabel("Ordonn�es");
		this.val = new JLabel("Valeur");
		this.absField = new JTextField(10);
		this.ordField = new JTextField(10);
		this.valField = new JTextField(10);
		this.bValider = new JButton("Valider");
		this.bEffacer = new JButton("Effacer");
		this.bTerminer = new JButton("Terminer");
		
		this.bValider.addActionListener(new ActionButton());
		this.bEffacer.addActionListener(new ActionButton());
		this.bTerminer.addActionListener(new ActionButton());
		
		this.setTitle(titre);
		
		this.setLayout(new BorderLayout());
		
		GridLayout grid = new GridLayout(2,3);
		JPanel gridPan = new JPanel();
		gridPan.setLayout(grid);
		this.getContentPane().add(gridPan, BorderLayout.CENTER);
		
		FlowLayout flow = new FlowLayout();JPanel flowPan = new JPanel();
		flowPan.setLayout(flow);
		this.getContentPane().add(flowPan, BorderLayout.SOUTH);
		
		gridPan.add(abs);
		gridPan.add(ord);
		gridPan.add(val);
		gridPan.add(absField);
		gridPan.add(ordField);
		gridPan.add(valField);
		flowPan.add(bValider);
		flowPan.add(bEffacer);
		flowPan.add(bTerminer);
		
		this.pack();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
	
	public static void main(String[] args) {
		InterfaceSwing frame1 = new InterfaceSwing("Saisie Donnees");
		frame1.setVisible(true);
	}

	public boolean isInt(String chaine) {
		try {
			Integer.parseInt(chaine);
		} catch (NumberFormatException e) {
            return false;
		}
		return true;
	}
	
	public boolean isDouble(String chaine) {
		try {
			Double.parseDouble(chaine);
		} catch (NumberFormatException e) {
            return false;
		}
		return true;
	}
	
	public class ActionButton implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			Object source = evt.getSource();
			
			if(source == bValider) {
				if(!isInt(absField.getText())) {
					absField.setBackground(Color.RED);
					return;
				}
				if(!isInt(ordField.getText())) {
					ordField.setBackground(Color.RED);
					return;
				}
				if(!isDouble(valField.getText())) {
					valField.setBackground(Color.RED);
					return;
				}
				absList.add(absField.getText());
				ordList.add(ordField.getText());
				valList.add(valField.getText());
				absField.setBackground(Color.WHITE);
				ordField.setBackground(Color.WHITE);
				valField.setBackground(Color.WHITE);
				absField.setText("");
				ordField.setText("");
				valField.setText("");
			}
			else if(source == bEffacer) {
				absField.setText("");
				ordField.setText("");
				valField.setText("");
				absField.setBackground(Color.WHITE);
				ordField.setBackground(Color.WHITE);
				valField.setBackground(Color.WHITE);
			}
			else if(source == bTerminer){
				try {
					FileWriter file = new FileWriter("DonneesSaisies.txt", true);
					for(int i=0; i<absList.size();i++) {
						file.write(absList.get(i) + ',' + ordList.get(i) + ' ' + valList.get(i) + '\n');
					}
					file.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}	
		}
	}
}
