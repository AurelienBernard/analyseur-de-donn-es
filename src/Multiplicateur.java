/**
  * Multiplicateur transmet la valeur multipliée par un facteur.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Multiplicateur extends Traitement {
	
	private double multiplicateur = 0;

	public Multiplicateur(double multi) {
		this.multiplicateur = multi;
	}

	@Override
	public void traiter(Position position, double valeur) {
		valeur = valeur*this.multiplicateur;
		super.traiter(position, valeur);
	}
}
