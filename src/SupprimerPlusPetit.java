/**
  * SupprimerPlusPetit supprime les valeurs plus petites qu'un seuil.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class SupprimerPlusPetit extends Traitement {

	private double seuil = 0;
	
	public SupprimerPlusPetit(double seuil) {
		this.seuil = seuil;
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		if(valeur > this.seuil) {
			super.traiter(position, valeur);
		}
	}

}
