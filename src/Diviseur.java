/**
  * Diviseur transmet la valeur divis� par un facteur.
  *
  * 
  */
public class Diviseur extends Traitement {
	
	private double quotient = 0;

	public Diviseur(double quotient) {
		this.quotient = quotient;
	}

	@Override
	public void traiter(Position position, double valeur) {
		valeur = valeur/this.quotient;
		super.traiter(position, valeur);
	}
}
