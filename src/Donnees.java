import java.util.ArrayList;

/**
  * Donnees enregistre toutes les données reçues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Donnees extends Traitement {

	private ArrayList<Position> posList = new ArrayList<Position>();
	private ArrayList<Double> valList = new ArrayList<Double>();
	
	@Override
	public void traiter(Position position, double valeur) {
		this.posList.add(position);
		this.valList.add(valeur);
		super.traiter(position, valeur);
	}

	
	public ArrayList<Double>getValeurList(){
		return this.valList;
	}
	
	public ArrayList<Position>getPositionList(){
		return this.posList;
	}
}
