import java.util.*;

/**
  * SommeParPosition 
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class SommeParPosition extends Traitement {

    private HashMap<String, Double> donnees = new HashMap<String, Double>();
    
    @Override
    public void traiter(Position position, double valeur) {
        this.donnees.merge("(" + position.x + "," + position.y + ")", valeur, Double::sum);
        super.traiter(position, valeur);
    }

}
