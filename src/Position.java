/** Définir une position.  */
public class Position {
	public int x;
	public int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
		// System.out.println("...appel à Position(" + x + "," + y + ")" + " --> " + this);
	}

	@Override public String toString() {
		return super.toString() + "(" + x + "," + y + ")";
	}
	
	@Override
	public boolean equals(Object position)
	{
		if(!(position instanceof Position))
			return false;
		
		Position p2 = (Position) position;
		
		return (this.x == p2.getX() && this.y == p2.getY());
	}
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}

	@Override
	public int hashCode() {
		int prime = 52;
		int result = 1;
		result = prime * result + this.x;
		result = prime * result + this.y;
		return result;
		
	}

}
