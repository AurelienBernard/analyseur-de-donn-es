import java.util.*;

/**
 * Maj indique pour chaque lot les positions mises à jour (ou ajoutées)
 * lors du traitement de ce lot.
 *
 * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class Maj extends Traitement {

	private Donnees donnees;

	@Override
	public void gererDebutLotLocal(String nomLot)
	{
		this.donnees = new Donnees();
		this.donnees.gererDebutLot(nomLot);
	}
	
	@Override
	public void traiter(Position position, double valeur) 
	{
		
		if(!this.donnees.getPositionList().contains(position))
			this.donnees.traiter(position, valeur);
		
		super.traiter(position, valeur);
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) 
	{		
		System.out.println("Position maj pour le lot (" + nomLot + ") : ");
		for(Iterator<Position> it = this.donnees.getPositionList().iterator(); it.hasNext(); )
		{
			Position p = it.next();
			
			System.out.println("\t Position = " + p);
		}
	}


}
