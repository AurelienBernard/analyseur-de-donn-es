import java.util.*;

/**
  * Positions enregistre toutes les positions, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Positions extends PositionsAbstrait {

	private ArrayList<Position> positions = new ArrayList<Position>();
	
	@Override
	public void traiter(Position position, double valeur) {
		this.positions.add(position);
		super.traiter(position, valeur);
	}
	
	
	/** Obtenir le nombre de positions mémorisées.
	 * @return le nombre de positions mémorisées
	 */	
	@Override
	public int nombre() {
		return this.positions.size();
	}

	/** Obtenir la ième position enregistrée.
	 * @param numero numéro de la position souhaitée (0 pour la première)
	 * @return la position de numéro d'ordre `numero`
	 * @exception IndexOutOfBoundsException si le numero est incorrect
	 */
	
	@Override
	public Position position(int indice) {
		return this.positions.get(indice);
	}

	/** Obtenir la fréquence d'une position dans les positions traitées.
	 * @param position la position dont on veut connaître la fréquence
	 * @return la fréquence de la position en paramètre
	 */
	
	@Override
	public int frequence(Position position) {
		int cpt = 0;
		for(Position pos : this.positions) {
			if(pos.equals(position))
				cpt++;
		}
		return cpt;
	}
	
}
